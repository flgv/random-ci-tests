Test gitlab CI workflow definition.

The goal is to have a CI definition that allows the following workflow:

 - `master` branch used for current working version of product
 - `feature/` branches for everything
 - `experimental/` branches for large code refactoring, where test breakage is expected. Basic code validation, such as linting or static analysis *must* still occur whithin these branches
 - `release/` tags for references within `master` that represent deployed code.

And the following CI conditions:

 - `master` and all non-experimental branches undergo unit tests, but only `master` undergoes integration tests *at the CI level*. Locally, all tests should be run whenever it makes sense in non-`master` branches.
 - `master` is the single source of truth and stability for the product's codes, so it must always undergo *all* tests.
 - `master` deploys to a staging environment, at all commits. It represents a stable, _latest/nightly/beta_ version of the product.
 - `experimental/` branches *NEVER* undergo unit or integration tests, but code must still be minimally usable, so compilation/static analisys must always run.
 - `release/` tags should undergo every test and validation, although redundant due to it occurring on `master` and all `release/` tags should be `master` commits. Nevertheless, since `release/` tags get deployed to production, it's best to be extra careful.

Assumptions:

 - Although the configuration can be easily adapted for such a use case, there is an implicit assumption that the releases are monotonically increasing. There are no backports of fixes or features.
 - This workflow expects `master` to be stable at all times, since all releases are tagged states of `master`.